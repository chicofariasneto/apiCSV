# apiCSV
💻📋 Api for uploading and reading .csv files, finally exporting the data in json... 

# What is it for
This is an template api to upload and reading .csv files and returning in json.

# How to use

Just run on terminal

```
    npm install
    npm install --only-dev
```

and after, run npm start. Now the API is running and just test it by sending a .csv file. You can use Insomnia to do the tests... In fact, I recommend that you use insomnia for testing.

You can also create a route that returns an html, add an inputFile to the Html, this will also serve ...

# Author

[chicofariasneto](https://github.com/chicofariasneto)

### License

MIT License

Copyright (c) 2020 Francisco Rodrigues

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
